<?php
// PUNK Client

define('CURL_URL','https://api.punkapi.com/v2/beers/');
define('CURL_LIMIT', 60);

Class Punk {

  private $curl_url;

  public function __construct()
  {
    $this->curl_url = CURL_URL;
  }

  public function getBeers()
  {
    // Ouverture cURL
    $cURLConnection = curl_init($this->curl_url . "?per_page=" . CURL_LIMIT);

    // Options
    curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

    // Recuperation data
    $result = curl_exec($cURLConnection);

    // Fermeture cURL
    curl_close($cURLConnection);

    return $result;
  }

  public function getBeer($id)
  {
    // Ouverture cURL
    $cURLConnection = curl_init($this->curl_url . $id);

    // Options
    curl_setopt($cURLConnection, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

    // Recuperation data
    $result = curl_exec($cURLConnection);

    // Fermeture cURL
    curl_close($cURLConnection);

    return $result;
  }
}
