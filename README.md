## Rappel de l'objectif du test de recrutement - Développeur Front-End

Réaliser une page web affichant une liste de bières (60 bières sous forme d'un catalogue de produits.

L'API a interroger est [PUNK API](https://punkapi.com),

Liste des informations à afficher :

- name
- tagline
- first_brewed
- image_url

Et dans chaque modale :

- name
- image_url
- description
- abv
- ingredients
- food pairings

Les maquettes à utiliser :

- [Liste des bières](https://projects.invisionapp.com/share/EAYO5TZTHDS#/screens/430821303)
- [Modale de la bière](https://projects.invisionapp.com/share/EAYO5TZTHDS#/screens/430821304)

## v1

Le travail demandé 😏.

Utilisation de `cURL` pour les appels API et `Axios`pour les appels à l'endpoint.

L'application a été développé en vuejs.

## v2 (version de prod / _prod)

Une autre version destinée à être mise en production et développé avec le framework Gridsome afin que Google puisse accéder correctement aux contenus (le code apparaissant en clair une fois l'application compilé).

Dans cette version, je n'ai pas utilisé le client.php et le endpoint.php. J'ai essayé, mais je n'y suis pas parvenu. Du coup, j'ai directement interrogé l'API. Cette version ne respecte pas complètement le sujet, mais je la trouve beaucoup plus abouti.

Dites-moi la version que vous préférez et ce que vous en pensez :).
