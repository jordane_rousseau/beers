let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
mix.setPublicPath('public');

mix.setResourceRoot('../../');

mix.js('src/js/vue.app.js', 'assets/js/vue.app.min.js')
    .sass('src/sass/custom.scss', 'assets/css/custom.min.css')
