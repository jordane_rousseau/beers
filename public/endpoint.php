<?php
require_once('../library/Client.php');

if(isset($_GET) && !empty($_GET)) {
  // Recuperation du get
  $get = $_GET['f'];

  // Creation de l'objet punk
  $punk = new Punk();

  // Recuperation donnees en fonction du get
  switch ($get) {
    case 'getAllData':
      $json = json_decode($punk->getBeers());

      $data = [];

      foreach ($json as $key => $value) {
        $data[] = array(
          'id' => $value->{"id"},
          'name' => $value->{"name"},
          'tagline' => $value->{"tagline"},
          'image_url' => $value->{"image_url"},
          'first_brewed' => $value->{"first_brewed"}
        );
      }

      $data = json_encode($data);

      break;

    case 'getData':
      $json = json_decode($punk->getBeer($_GET['id']));

      $data = [];

      foreach ($json as $key => $value) {
        $data[] = array(
          'id' => $value->{"id"},
          'name' => $value->{"name"},
          'tagline' => $value->{"tagline"},
          'image_url' => $value->{"image_url"},
          'first_brewed' => $value->{"first_brewed"},
          'abv' => $value->{"abv"},
          'ibu' => $value->{"ibu"},
          'ebc' => $value->{"ebc"},
          'srm' => $value->{"srm"},
          'ingredients' => $value->{"ingredients"}
        );
      }

      $data = json_encode($data);

      break;
    default:
      $data = null;
  }

  // Affichage du resultat
  print_r($data);
} else {
  print_r ("An unknown error has occurred");
}
