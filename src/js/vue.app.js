/* Vue App JS
================================================== */

import Vue from "vue";
import axios from "axios";
import VueMasonry from 'vue-masonry-css';
import VueScrollReveal from 'vue-scroll-reveal';

Vue.use(VueScrollReveal);
Vue.use(VueMasonry);

// Vue js App
const url = "endpoint.php";

const vm = new Vue({
  el: '#wrapper',
  data: {
    beersReady: false,
  	showModal: false,
    beers: null,
  	item_beer: null,
  },
  mounted() {
    this.requestData()
  },
  methods: {
    async requestData() {
      axios.get(url + "?f=getAllData").then(response => {
  		    this.beers = response.data;
          this.beersReady = true;
          window.scrollTo(0, 0);
  	  })
    },
  	openModal: function(id) {
  	  axios.get(url + "?f=getData&id=" + id).then(response => {
  		    this.item_beer = response.data;
          this.showModal = true;
          window.scrollTo(0, 0);
  	  })
  	},
    closeModal: function() {
      this.showModal = false;
    }
  },
});
